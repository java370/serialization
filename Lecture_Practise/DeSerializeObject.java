import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeSerializeObject {
    public static void main(String[] args) {
        // String serializedFileName = "MyObject.ser";
        String serializedFileName = "binary.dat";

        try {
            FileInputStream fileStream = new FileInputStream(serializedFileName);
            ObjectInputStream os = new ObjectInputStream(fileStream);
            Object one = os.readObject();
            TestObject t1 = (TestObject) one;
            System.out.println(t1);
            os.close();
        } catch (FileNotFoundException e) {
            System.out.println(serializedFileName + " File Not Found!");
            // e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Unable to read from " + serializedFileName);
            // e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Class Not Found!");
            // e.printStackTrace();
        }

    }
}
