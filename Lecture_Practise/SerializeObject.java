import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializeObject {
    public static void main(String[] args) throws Exception {
        String outputFileName = "MyObject.ser";
        FileOutputStream fileStream = new FileOutputStream(outputFileName);

        ObjectOutputStream os = new ObjectOutputStream(fileStream);

        TestObject t1 = new TestObject("Farid", 22);

        System.out.println(t1);
        os.writeObject(t1);
        os.close();

    }
}

class TestObject implements Serializable {
    private String name;
    private int age;

    public TestObject(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String toString() {
        return "name : " + name + "; age : " + age;
    }
}