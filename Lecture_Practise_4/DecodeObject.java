import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class DecodeObject {
    public static void main(String[] args) {
        if (args.length == 0 || (args.length < 1 || args.length > 1)) {
            System.out.println("Argument Error!");
        } else {
            File sourceFile = new File(args[0]);
            if (!sourceFile.isFile()) {
                System.out.println("This is not a File!");
            } else {
                try {
                    RetriveObject.getObject(sourceFile);
                } catch (FileNotFoundException e) {
                    System.out.println("File Not Found!");
                } catch (IOException e) {
                    System.out.println("I/O error!");
                } catch (IllegalArgumentException e) {
                    System.out.println("Buffer Error!");
                }
            }
        }
    }
}

class RetriveObject {
    public static void getObject(File sourceFile) throws FileNotFoundException, IOException, IllegalArgumentException {

        BufferedReader br = new BufferedReader(new FileReader(sourceFile));
        String line = null;
        List<Person> getPerson = new ArrayList<>();

        while ((line = br.readLine()) != null) {
            String[] tmp = line.split(",");
            getPerson.add(new Person(Long.parseLong(tmp[0]), tmp[1], Integer.parseInt(tmp[2])));
        }
        br.close();

        System.out.println("Retrived Data!");
        for (Person person : getPerson) {
            System.out.println(person.toString());
        }

    }
}