import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class EncodeObject {
    public static void main(String[] args) {

        // Create object of person class
        List<Person> p = new ArrayList<>();
        p.add(new Person(78976543, "mikimini", 20));
        p.add(new Person(98765, "cinipini", 30));
        p.add(new Person(9223372036854775807L, "cinipini", 30));
        p.add(new Person(9223372036854722222L, "binikini", 40));

        if (args.length == 0 || (args.length < 1 || args.length > 1)) {
            System.out.println("Argument Error!");
        } else {
            String sourceFile = args[0];
            try {
                FileWriter myWriter = new FileWriter(sourceFile);
                for (Person person : p) {
                    myWriter.write(person.toString());
                    myWriter.write('\n');
                }
                myWriter.close();
            } catch (FileNotFoundException e) {
                System.out.println("File Not Found!");
            } catch (IOException e) {
                System.out.println("I/O error!");
            } catch (SecurityException e) {
                System.out.println("Access Denied! System is protected!");
            }

        }
    }
}

class Person implements Serializable {
    private long id;
    private String name;
    private int age;

    public Person(long id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }

    public long getID() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public long getAge() {
        return this.age;
    }

    public String toString() {
        return id + "," + name + "," + age;
    }
}
