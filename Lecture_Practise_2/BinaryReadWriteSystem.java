import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class BinaryReadWriteSystem {
    public static void main(String[] args) {
        File f1 = new File(args[0]);
        File f2 = new File(args[1]);

        if (!f1.isFile()) {
            System.out.println("This is not a file.");
        } else if (f1.isDirectory() || f2.isDirectory()) {
            System.out.println("This is a drectory.");
        } else {
            try {
                RW_System.sys_exe(f1, f2);
            } catch (FileNotFoundException e) {
                System.out.println("File Not Found!");
            } catch (IOException e) {
                System.out.println("Input Output Error!");
            } catch (Exception e) {
                System.out.println("Input Output Error!");
            }
        }

    }
}

class RW_System {
    public static void sys_exe(File f1, File f2) throws Exception {

        BufferedInputStream bfi = new BufferedInputStream(new FileInputStream(f1));
        BufferedOutputStream bfo = new BufferedOutputStream(new FileOutputStream(f2));
        int c = 0;
        while ((c = bfi.available()) != 0) {
            byte[] byteArr = new byte[bfi.available()];
            bfi.read(byteArr);
            // String tmp = Integer.toBinaryString(c);
            // int k = Integer.parseInt(tmp);
            // System.out.print(k);
            bfo.write(byteArr);
        }
        bfi.close();
        bfo.close();
    }
}