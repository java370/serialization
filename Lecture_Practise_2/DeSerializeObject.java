import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeSerializeObject {
    public static void main(String[] args) {
        // String serializedFileName = "MyObject.ser";
        String serializedFileName = "binary.dat";
        FileInputStream fileStream = null;
        ObjectInputStream os = null;

        try {
            fileStream = new FileInputStream(serializedFileName);
            os = new ObjectInputStream(fileStream);
            Object one = os.readObject();
            TestObject t1 = (TestObject) one;
            System.out.println(t1);
            

        } catch (FileNotFoundException e) {
            System.out.println(serializedFileName + " File Not Found!");
            // e.printStackTrace();
        } catch (IOException e) {
            System.out.println("Unable to read from " + serializedFileName);
            // e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Class Not Found!");
            // e.printStackTrace();
        } finally {
            try {
                os.close();
            } catch (IOException e) {
                System.out.println("Unable to Close file!");
            }

        }

    }
}
