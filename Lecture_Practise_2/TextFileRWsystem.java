import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class TextFileRWsystem {
    public static void main(String[] args) {

        File f1 = new File(args[0]);
        File f2 = new File(args[1]);

        if (!f1.isFile()) {
            System.out.println("This is not a file.");
        } else if (f1.isDirectory() || f2.isDirectory()) {
            System.out.println("This is a drectory.");
        } else {
            try {
                RW_System.system_execution(f1, f2);
            } catch (FileNotFoundException e) {
                System.out.println("File Not Found!");
            } catch (IOException e) {
                System.out.println("Input Output Error!");
            } catch (Exception e) {
                System.out.println("Input Output Error!");
            }
        }

    }
}

class RW_System {
    public static void system_execution(File f1, File f2) throws Exception {

        BufferedReader br = new BufferedReader(new FileReader(f1));
        BufferedWriter bw = new BufferedWriter(new FileWriter(f2));
        int c;
        while ((c = br.read()) != -1) {
            bw.write((char) c);
        }
        br.close();
        bw.close();
    }
}
