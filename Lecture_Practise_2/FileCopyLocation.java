import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopyLocation {
    public static void main(String[] args) {
        if ((args.length < 2 || args.length > 2)) {
            System.out.println("Incorrect Arguments!");
        } else {
            try {
                final long startTime = System.currentTimeMillis();
                File sourceFile = new File(args[0]);
                File sinkFile = new File(args[1]);
                CopyFunction.DoCopy(sourceFile, sinkFile);
                final long endTime = System.currentTimeMillis();
                System.out.println("Total execution time: " + 1.0 * (endTime - startTime) / 1000 + " seconds!");
            } catch (FileNotFoundException e) {
                System.out.println("File Not Found!");
            } catch (IOException e) {
                System.out.println("File Not Found!");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}

class CopyFunction {
    public static void DoCopy(File sourceFile, File sinkFile) throws FileNotFoundException, IOException, Exception {

        FileInputStream fis = new FileInputStream(sourceFile);
        DataInputStream dis = new DataInputStream(fis);
        BufferedInputStream bis = new BufferedInputStream(dis);

        FileOutputStream fos = new FileOutputStream(sinkFile);
        DataOutputStream dos = new DataOutputStream(fos);
        BufferedOutputStream bos = new BufferedOutputStream(dos);

        int availableBits = 0;
        int c = 0;

        while ((availableBits = bis.available()) != 0) {

            byte[] byteToRead = new byte[availableBits];

            System.out.println("iteration " + ++c + " || Available Byets : " + availableBits + " || Byte Array Size : "
                    + byteToRead.length);

            bis.read(byteToRead);
            for (int i = 0; i < byteToRead.length; i++) {
                bos.write(byteToRead[i]);
            }
        }
        dos.flush();
        dos.close();
        fos.close();
        dis.close();
        fis.close();
    }
}