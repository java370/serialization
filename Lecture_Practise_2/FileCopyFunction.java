import java.io.DataInput;
import java.io.DataInputStream;
import java.io.DataOutput;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Scanner;

public class FileCopyFunction {
    public static void main(String[] args) throws Exception {
        
        // Scanner sc = new Scanner(System.in);
        // System.out.println("Source File Name : ");
        // String sourceFile = sc.next();
        String sourceFile = args[0];
        // System.out.println("Sink File Name : ");
        // String sinkFile = sc.next();
        String sinkFile = args[1];
        final long startTime = System.currentTimeMillis();
        CopyFunction.DoCopy(sourceFile, sinkFile);
        final long endTime = System.currentTimeMillis();
        System.out.println("Total execution time: " + (endTime - startTime));
        // sc.close();
    }
}

class CopyFunction {
    public static void DoCopy(String sourceFile, String sinkFile) throws Exception {
        String file = sourceFile;
        String file_modified = "modified_" + sinkFile;

        FileInputStream fis = new FileInputStream(file);
        DataInputStream dis = new DataInputStream(fis);

        FileOutputStream fos = new FileOutputStream(file_modified);
        DataOutputStream dos = new DataOutputStream(fos);

        int availableBits = 0;
        while ((availableBits = dis.available()) != 0) {
            System.out.print("Total available Byets : " + availableBits);
            byte[] byteToRead = new byte[availableBits];
            dis.read(byteToRead);

            System.out.println(
                    " After available Byets : " + dis.available() + " Current Byte array Size : " + byteToRead.length);

            for (int i = 0; i < byteToRead.length; i++) {
                dos.write(byteToRead[i]);
            }
        }
        dos.flush();
        dos.close();
        fos.close();
        dis.close();
        fis.close();
    }
}