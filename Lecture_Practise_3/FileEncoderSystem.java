import java.io.*;

public class FileEncoderSystem {
    public static void main(String[] args) {

        if (args.length == 0 || (args.length < 1 || args.length > 1)) {
            System.out.println("Argument Error!");
        } else {
            File sourceFile = new File(args[0]);
            if (!sourceFile.isFile()) {
                System.out.println("This is not a File!");
            } else {
                try {
                    SourceToTest.dump(sourceFile);
                } catch (FileNotFoundException e) {
                    System.out.println("File Not Found!");
                } catch (IOException e) {
                    System.out.println("I/O error!");
                } catch (SecurityException e) {
                    System.out.println("Access Denied! System is protected!");
                }
            }
        }
    }
}

class SourceToTest {
    public static void dump(File sourceFile) throws FileNotFoundException, IOException, SecurityException {

        FileInputStream fis = new FileInputStream(sourceFile);
        DataInputStream dis = new DataInputStream(fis);
        BufferedInputStream bis = new BufferedInputStream(dis);

        int availableBits = 0;
        while ((availableBits = bis.available()) != 0) {
            byte[] byteToRead = new byte[availableBits];
            bis.read(byteToRead);
            for (int i = 0; i < byteToRead.length; i++) {
                System.out.println(byteToRead[i]);
            }
        }
        dis.close();
        fis.close();
    }
}
