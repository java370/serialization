import java.io.*;

/**
 * Mohammad Abu Yousuf Sajal
 * jpg, pdf, mp3, mp4 DECODER
 */

public class FileDecoderSystem {
    public static void main(String[] args) {
        if (args.length == 0 || (args.length < 1 || args.length > 1)) {
            System.out.println("Argument Error!");
        } else {
            File sourceFile = new File(args[0]);
            if (!sourceFile.isFile()) {
                System.out.println("This is not a File!");
            } else {
                try {
                    CovertionFunction.retriveTextData(sourceFile);
                } catch (FileNotFoundException e) {
                    System.out.println("File Not Found!");
                } catch (IOException e) {
                    System.out.println("I/O error!");
                } catch (IllegalArgumentException e) {
                    System.out.println("Buffer Error!");
                }
            }
        }
    }
}

class CovertionFunction {

    public static void retriveTextData(File sourcefFile)
            throws FileNotFoundException, IOException, IllegalArgumentException {
        // ReadFile as a Text
        BufferedReader br = new BufferedReader(new FileReader(sourcefFile));
        String line = null;

        // OUTPUT FILE NAME;
        String sinkFile = "Decoded_File";
        // Naming convention Ex : 'cat.jpg' --> 'decoded_cat'
        // String sinkFile = "decoded_" +
        // sourcefFile.getName().substring(0,sourcefFile.getName().indexOf("."));

        FileOutputStream fos = new FileOutputStream(sinkFile);
        DataOutputStream dos = new DataOutputStream(fos);
        BufferedOutputStream bos = new BufferedOutputStream(dos);

        while ((line = br.readLine()) != null) {
            Integer n = Integer.parseInt(line);
            bos.write(n);
        }
        br.close();
        bos.close();

    }
}
