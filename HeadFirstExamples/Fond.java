import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Fond implements Serializable {

    // now make this Object variable TRANSIENT
    // Escape this variable to be serialized
    transient private Duck duck = new Duck();

    // Other Fields which should implement serialization
    private Gond gond;
    // private static Gond gond = new Gond(234, 88);

    public static void main(String[] args) {
        Fond ff = new Fond();
        ff.gond = new Gond(234, 897);
        try {

            FileOutputStream fs = new FileOutputStream("Fond.ser");
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(ff.gond);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

class Duck {
    // No code
    // Not implements Serialization
}

// this Class should implemnt serialization and all the objects created from it
class Gond implements Serializable {
    private int width;
    private int height;

    public Gond(int width, int height) {
        this.width = width;
        this.height = height;
    }
}
