import java.io.*;

public class SimpleReadFile {
    public static void main(String[] args) {
        try {
            File myFile = new File("Foo.txt");
            FileReader fileReader = new FileReader(myFile);
            // Make More Efficient with BufferReader
            BufferedReader reader = new BufferedReader(fileReader);

            String line = null;
            while ((line = reader.readLine()) != null) {
                System.out.println(line);
            }
            fileReader.close();
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
