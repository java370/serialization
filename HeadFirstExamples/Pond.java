import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Pond implements Serializable {

    private static Duck duck = new Duck();

    public static void main(String[] args) {
        try {
            FileOutputStream fs = new FileOutputStream("Pond.ser");
            ObjectOutputStream os = new ObjectOutputStream(fs);
            os.writeObject(duck);
            os.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

class Duck {
    // No code
}
