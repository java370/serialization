// https://www.geeksforgeeks.org/customized-serialization-and-deserialization-in-java/?ref=rp

/**During serialization, there may be data loss if we use the ‘transient’ keyword. 
 * ‘Transient’ keyword is used on the variables which we don’t want to serialize. 
 * But sometimes, it is needed to serialize them in a different manner than the default serialization 
 * (such as encrypting before serializing etc.), in that case, we have to use custom serialization and deserialization. 
 * 
 * */

import java.io.*;

class GfgAccount implements Serializable {

    // Performing customized serialization using the below two methods:

    // this method is executed by jvm when writeObject() on
    // Account object reference in main method is executed by jvm.

    private void writeObject(ObjectOutputStream oos) throws Exception {
        // to perform default serialization of Account object.
        oos.defaultWriteObject();

        // epwd (encrypted password)
        // String epwd = "321"+pwd;
        String epwd = pwd;

        // writing encrypted password to the file
        oos.writeObject(epwd);
    }

    // this method is executed by jvm when readObject() on
    // Account object reference in main method is executed by jvm.
    private void readObject(ObjectInputStream ois) throws Exception {
        // performing default deserialization of Account object
        ois.defaultReadObject();

        // deserializing the encrypted password from the file
        String epwd = (String) ois.readObject();

        // decrypting it and saving it to the original password
        // string starting from 3rd index till the last index
        // pwd = epwd.substring(3);

        // No Decrypting
        pwd = epwd;
    }

    String username = "sajal_admin";
    transient String pwd = "5432";

}

public class CustomSerialization {
    public static void main(String[] args) throws Exception {
        GfgAccount gfg_g1 = new GfgAccount();

        System.out.println("Username : " + gfg_g1.username +
                "    Password : " + gfg_g1.pwd);

        FileOutputStream fos = new FileOutputStream("abc.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(gfg_g1);

        FileInputStream fis = new FileInputStream("abc.ser");
        ObjectInputStream ois = new ObjectInputStream(fis);

        // readObject() method present GfgAccount class
        // will be automatically called by jvm
        // JVM cant get transient instance variables
        GfgAccount gfg_g2 = (GfgAccount) ois.readObject();

        System.out.println("Username : " + gfg_g2.username +
                "      Password : " + gfg_g2.pwd);

    }
}
