import java.io.FileWriter;
import java.io.IOException;

public class SimpleWriteFile {
    public static void main(String[] args) {
        try {
            FileWriter writer = new FileWriter("Foo.txt");
            writer.write("Hi I am SAJAL");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
