import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class DungeonGame implements Serializable {
    public int x = 3;
    transient long y = 4; // This state won't be saved
    private short z = 5;

    int getX() {
        return this.x;
    }

    long getY() {
        return this.y;
    }

    short getZ() {
        return this.z;
    }
}

class DungeonTest {
    public static void main(String[] args) {
        DungeonGame d = new DungeonGame();
        DungeonGame output = new DungeonGame();
        System.out.println("Initial Stages of Data");
        System.out.println(d.getX() + " " + d.getY() + " " + d.getZ());

        try {
            // File write in a stream
            FileOutputStream fo = new FileOutputStream("DumpData.ser");
            ObjectOutputStream Oo = new ObjectOutputStream(fo);
            Oo.writeObject(d);
            Oo.close();
            // File read using stream
            FileInputStream fi = new FileInputStream("DumpData.ser");
            ObjectInputStream Oi = new ObjectInputStream(fi);
            output = (DungeonGame) Oi.readObject();
            Oi.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("Read From Files");
        System.out.println(output.getX() + " " + output.getY() + " " + output.getZ());
    }
}
