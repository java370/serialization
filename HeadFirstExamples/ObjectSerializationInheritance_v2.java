
/**
 * If a superclass is not serializable, then subclass can still be serialized
 * Even though the superclass doesn’t implement a Serializable interface, we can
 * serialize subclass objects if the subclass itself implements a Serializable
 * interface. So we can say that to serialize subclass objects, superclass need
 * not be serializable. But what happens with the instances of superclass during
 * serialization in this case. The following procedure explains this.
 * 
 * Case 2(a): What happens when a class is serializable, but its superclass is
 * not?
 * Serialization: At the time of serialization, if any instance variable
 * inherits from the non-serializable superclass, then JVM ignores the original
 * value of that instance variable and saves the default value to the file.
 * 
 * De- Serialization: At the time of de-serialization, if any non-serializable
 * superclass is present, then JVM will execute instance control flow in the
 * superclass. To execute instance control flow in a class, JVM will always
 * invoke the default(no-arg) constructor of that class. So every
 * non-serializable superclass must necessarily contain a default constructor.
 * Otherwise, we will get a runtime exception.
 */

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

// superclass A
// A class doesn't implement Serializable
// interface.
class A {
    int i;

    // parameterized constructor
    public A(int i) {
        this.i = i;
    }

    // default constructor
    // this constructor must be present
    // otherwise we will get runtime exception
    public A() {
        i = 50;  // in DeSerialization this value will invoke
        System.out.println("A's class constructor called");
    }
}

// subclass B
// implementing Serializable interface
class B extends A implements Serializable {
    int j;

    // parameterized constructor
    public B(int i, int j) {
        super(i);
        this.j = j;
    }
}

// Driver class
public class ObjectSerializationInheritance_v2 {
    public static void main(String[] args) throws Exception {
        B b1 = new B(9999, 6666);

        System.out.println("i = " + b1.i);
        System.out.println("j = " + b1.j);

        // Serializing B's(subclass) object

        // Saving of object in a file
        FileOutputStream fos = new FileOutputStream("abc.ser");
        ObjectOutputStream oos = new ObjectOutputStream(fos);

        // Method for serialization of B's class object
        oos.writeObject(b1);

        // closing streams
        oos.close();
        fos.close();

        System.out.println("Object has been serialized");

        // De-Serializing B's(subclass) object

        // Reading the object from a file
        FileInputStream fis = new FileInputStream("abc.ser");
        ObjectInputStream ois = new ObjectInputStream(fis);

        // Method for de-serialization of B's class object
        B b2 = (B) ois.readObject();

        // closing streams
        ois.close();
        fis.close();

        System.out.println("Object has been deserialized");

        System.out.println("i = " + b2.i);
        System.out.println("j = " + b2.j);
    }
}
